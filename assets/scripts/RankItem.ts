const { ccclass, property } = cc._decorator;

@ccclass
export default class RankItem extends cc.Component {

    @property(cc.Label)
    private rankingLabel: cc.Label = null;

    @property(cc.Sprite)
    private avatarSprite: cc.Sprite = null;

    @property(cc.Label)
    private nicknameLabel: cc.Label = null;

    @property(cc.Label)
    private scoreLabel: cc.Label = null;


    public set(ranking: number, user: UserGameData) {
        this.rankingLabel.string = ranking.toString();
        this.nicknameLabel.string = user.nickname;
        this.scoreLabel.string = user.KVDataList[0].value.toString();
        this.updateAvator(user.avatarUrl);
    }

    private updateAvator(url: string) {
        let image = wx.createImage();
        image.onload = () => {
            let texture = new cc.Texture2D();
            texture.initWithElement(image);
            texture.handleLoadedTexture();
            this.avatarSprite.spriteFrame = new cc.SpriteFrame(texture);
        };
        image.src = url;
    }
}
