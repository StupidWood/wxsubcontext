# 微信开放数据域（好友排行榜）

#### 介绍

一个通用的微信排行榜项目模板，可直接使用。

#### 环境

引擎：Cocos Creator 2.3.2

语言：TypeScript

#### 关于

作者：陈皮皮（ifaswind）

公众号：文弱书生陈皮皮

![weixin](https://gitee.com/ifaswind/image-storage/raw/master/weixin/qrcode.png)